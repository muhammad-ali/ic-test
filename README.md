# README #

To run this code you should have ruby version 2 or higher must be installed on your system.

### Run the code ###

* Clone this repo
* Open terminal and go to the directory.
* Run the command
<code>
    ruby main
</code>
* you will see the output result in output.txt file.

### To Run Test cases ###

* Open terminal and go to the directory.
* Run the command
<code>
    ruby test
</code>

### Output File ###

The sample Output file is in the directory with the name output.txt
