require_relative "person"
require "test/unit"

class TestPerson < Test::Unit::TestCase

  def test_should_invite
    # Coordinated of dublin are used
    person = Person.new("Fadi", 41, "53.3874288", "-6.2546902090338605")
    assert_equal(true, person.invite? )
  end

  def test_should_not_invite
    # Coordinated of Galway are used
    person = Person.new("Muhammad Ali", 42, "53.284150600000004", "-9.037836324751463")
    assert_equal(false, person.invite? )
  end

end
