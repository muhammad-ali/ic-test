require 'json'
require_relative 'person'
module FileIO

  def FileIO.load_people
    file = File.open("customers.txt")
    file_data = file.readlines.map(&:chomp)
    person_array = []
    file_data.each do |fd|
      u = JSON.parse(fd)
      person_array << Person.new(u["name"], u["user_id"], u["latitude"], u["longitude"])
    end
    file.close
    person_array
  end

  def FileIO.output_file(people)
    file = File.open("output.txt", "w")
    people.each do |person|
      u = "{'user_id': #{person.user_id}, 'name': '#{person.name}'}\n"
      file.write u
    end
    file.close
  end
end
