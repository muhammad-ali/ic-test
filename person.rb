class Person
  OFFICE_LAT = 53.339428
  OFFICE_LONG = -6.257664
  EARTH_RADIOUS = 6371

  attr_accessor :user_id, :name
  def initialize(name, user_id, latitude, longitude)
    # Instance variables
    @name = name
    @user_id = user_id
    @latitude = latitude.to_f
    @longitude = longitude.to_f
  end


  def print_user
    p "#{@user_id} #{@name} #{distance_from_office} #{invite?}"
  end

  def distance_from_office
    distance_in_km
  end

  def invite?
    distance_in_km <= 100 ? true : false
  end

  private
   def long_absolute_difference
     (OFFICE_LONG - @longitude).abs
   end

   def central_angle
     Math.acos( (Math.sin(@latitude) * Math.sin(OFFICE_LAT)) + (Math.cos(@latitude)* Math.cos(OFFICE_LAT)* Math.cos(long_absolute_difference)))
   end

   def central_angle_radian
     degrees = central_angle
     radians = (degrees * Math::PI) / 180
   end

   def distance_in_km
     EARTH_RADIOUS * central_angle_radian
   end
end
