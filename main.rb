require_relative 'person'
require_relative 'fileio'

def guest_list(people)
  guests = []
  people.each do |person|
    guests << person if person.invite?
  end
  guests
end

def main
  person_array = FileIO.load_people
  person_array = guest_list(person_array)
  person_array = person_array.sort_by{ |guest| guest.user_id }
  FileIO.output_file(person_array)
end

main
